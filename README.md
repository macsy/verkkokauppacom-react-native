# Verkkokauppa.com React Native demo

## Latest Android build:
https://expo.io/builds/f77c9f9f-82a6-40fd-a941-d8e38090cc9b

## Requirements

* Node

App compiles live mobile versions using Expo. Install the app and scan QR app after starting up expo compiler on PC:

```
npm install
exp start
```

More About Expo: https://expo.io

![alt text](assets/images/screenshot.png)
