import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { WebBrowser } from 'expo';


export default class SimpleToggleButton extends React.Component {

  constructor(props){
    super(props)
    _onStateChange = this._onStateChange.bind(this)
  }

  _onStateChange(newState) {
    
  }
  state={
    toggle:false
  }
  _onPress(){
    const newState = !this.state.toggle;
    this.setState({toggle:newState})
    this.props.onStateChange && this.props.onStateChange(newState)
  }
  render() {
    const {toggle} = this.state;
    const textValue = toggle?"ON":"OFF";
    const buttonBg = toggle?"dodgerblue":"white";
    const textColor = toggle?"white":"black";
    return (
        <TouchableOpacity onPress={()=>this._onPress()} 
        style={{margin:10,flex:1,height:60,
            backgroundColor:buttonBg,
        justifyContent:'center'}}>
        <Text style={{color:textColor,textAlign:'center',
        fontSize:16}}>{textValue}</Text>
        </TouchableOpacity>
    );
  }
}
