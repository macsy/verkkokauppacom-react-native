import React, {
    Component,
    PropTypes,
    StyleSheet,
    Text,
    View,
  } from 'react-native'

import Drawer from 'react-native-drawer'

export default class Drawer extends React.Component {
  state={
    drawerOpen: false,
    drawerDisabled: false,
  };
  closeControlPanel = () => {
    this._drawer.close()
  };
  openControlPanel = () => {
    this._drawer.open()
  };
  render () {
    return (
    <Drawer
        ref={(ref) => this._drawer = ref}
        type="static"
        content={
        <ControlPanel closeDrawer={this.closeDrawer} />
        }
        acceptDoubleTap
        styles={{main: {shadowColor: '#000000', shadowOpacity: 0.3, shadowRadius: 15}}}
        onOpen={() => {
        console.log('onopen')
        this.setState({drawerOpen: true})
        }}
        onClose={() => {
        console.log('onclose')
        this.setState({drawerOpen: false})
        }}
        captureGestures={false}
        tweenDuration={100}
        panThreshold={0.08}
        disabled={this.state.drawerDisabled}
        openDrawerOffset={(viewport) => {
        return 100
        }}
        closedDrawerOffset={() => 50}
        panOpenMask={0.2}
        negotiatePan
        >
    </Drawer>
    )
  }
}