import React, { Component } from 'react';

import {
    Image,
    StyleSheet,
    View,
    Text
  } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Accordion from 'react-native-collapsible/Accordion';


const SECTIONS = [
  {
    title: 'Read More',
    content: 'Blackstorm 4K MSI ‐pöytätietokone pelikäyttöön',
    moreContent: 'Vakuuttavaan Fractal Design Define C -koteloon kasattu Blackstorm 4K MSI -pelitietokone yllättää! Ensiluokkaisesta suorituskyvystä vastaavat Intelin Core i7-8700K -suoritin, NVIDIAn GeForce GTX1080 -näytönohjain, 32 Gt nopeaa DDR4-muistia sekä Windows 10 Pro. Tallennustiloista vastaavat 500 Gt M.2 SSD- ja 2 Tt mekaaninen kiintolevy. Voit siis asentaa ohjelmat nopealle M.2 SSD -kiintolevylle ja käyttää 2 Tt -tallennustilaa kuville, videoille, musiikille ja muille dokumenteille.'
    +'\n\nBlackstorm 4K MSI on rakennettu erittäin suorituskykyiseksi ja se kykenee pyörittämään viimeisimpiä pelejä sulavasti korkeilla grafiikka-asetuksilla. Kotelon ilmanvaihtoon ja äänenvoimakkuutten on panostettu, ja tietokone pysyy hiljaisena ja viileänä kohtalaisessakin rasituksessa. Monipuoliset liitännät kuten USB 3.0, DisplayPort, HDMI 2.0 takaavat erinomaisen liitettävyyden mihin tahansa lisälaitteeseen nyt ja tulevaisuudessa. Emolevyssä on lisäksi reilusti tilaa laajennuksille, katso tarkemmat tiedot ominaisuuslistasta.'
    +'\n\nTämäkin Blackstorm-pelitietokone on rakennettu parhaista mahdollisista saatavilla olevista komponenteista, ja sen ovat käyttöä vaille valmiiksi koonneet Verkkokauppa.comin ammattilaiset. Blackstorm-tietokoneet ovat varma valinta reilusti tehoa vaativaan ammattikäyttöön esimerkiksi työskentelyyn Adobe-ohjelmistojen kanssa tai CAD-suunnitteluun.'
    +'\n\nKaikki Blackstorm-tietokoneet toimitetaan puhtaalla käyttöjärjestelmällä ilman konetta turhaan rasittavia bloatware-sovelluksia - näin saat kaiken suorituskyvyn sinne missä sitä oikeasti tarvitaan. Tehokkaiden komponenttien ansiosta tämä Blackstorm-pelitietokone pysyy suorituskykyisenä vielä pitkälle tulevaisuuteen. Käsittelemme myös huoltotapaukset nopeasti ja tehokkaasti paikan päällä Verkkokauppa.comissa. Turvallinen 36 kk takuu.',
    imagesrc: require("../assets/images/blackstorm.jpeg"),
    height:120,
    width:80,
    price:"2 299,90 €"
  },
  {
    title: 'Read More',
    content: 'Dyson AM09 Hot & Cool tuuletin',
    moreContent: 'Talvella tehokas lämmitin – kesällä tehokas tuuletin. Käyttökelpoinen koko vuoden ympäri! Air Multiplier teknologia tuottaa tasaisen ja tehokkaan ilmavirran. Entistä hiljaisempi ja tehokkaampi malli. Uusi Jet Focus toiminto mahdollistaa ilmavirran (kuuman tai kylmän) kohdistamisen yhteen paikkaan (henkilökohtainen tila) tai laajemmalle (koko huoneen tila).' 
    +'\n\nNäkyviä lämmityselementtejä tai nopeasti pyöriviä lapoja ei ole, joten tuote on turvallinen käyttää. Se myös sammuu automaattisesti jos laite kaatuu. Laite kallistuu 10° suuntaansa oman painopisteensä suhteen ja pysyy paikoillaan ilman kiristimiä.Laitteessa on mukana ohjelmoitava ajastin ja kaukosäädin.',
    imagesrc: require("../assets/images/tuuletin.jpg"),
    height:120,
    width:35,
    price:"399,90 €"
  },
  {
    title: 'Read More',
    content: 'Samsung QE65Q7F 65" Smart 4K Ultra HD QLED ‐televisio',
    moreContent: 'Samsung QE65Q7F on huippuunsa viritetty QLED-televisio, jonka edistyksellinen tekniikka takaa upean kuvanlaadun. 4K-resoluutioisella televisiolla näet jokaisen varjon ja värisävyn juuri niinkuin ne on tarkoitettu. Valaistuksesta riippumatta erotat syvät mustan sävyt ja pienet yksityiskohdat selkeästi.'
    +'\n\n65-tuumaisessa televisiossa on teräväpiirtokanavia tukevat tuplavirittimet DVB-T2, DVB-C HD ja DVB-S2. Siinä on myös CI+-liitäntä maksu-tv-kanavia varten ja voit kiinnittää sen seinälle VESA-standardin mukaisella telineellä.'    
    +'\n\nQLED käyttää uutta Quantum Dot -materiaalia. Se on entistäkin tehokkaampaa ja tuottaa enemmän valoa ja suuremman väriavaruuden kuin perinteinen televisio. Q HDR1500 tarjoaa jopa 1500 Nit maksimikirkkauden ja Supreme UHD Dimming parantaa kuvan yksityiskohtia.'
    +'\n\n2017 Smart Hub kerää kaikki liitetyt videolähteet kätevästi samaan paikkaan. Oli kyse sitten suorista TV-lähetyksistä, Smart-sovelluksesta, Blu-ray-soittimesta tai pelikonsolista – löydät kaikki samasta helppokäyttöisestä valikosta.'
    +'\n\nTV tunnistaa automaattisesti useimmat ulkoiset laitteet ja nimeää ne esimerkiksi "Blu-ray" sen sijaan että lähteen näkyisi "HDMI3".'
    +'\n\nTelevisiossa on tietenkin myös sisäänrakennettu WiFi ja voit toistaa mediatiedostot näppärästi mobiililaitteeltasi. Voit myös liittää televisioon Bluetooth-kuulokkeet ja mukana tulee kätevä One Connect -liitäntäboksi. Kaikki oheislaitteet liitetään One Connect -boksiin, joka puolestaan liitetään televisioon huomaamattomalla viisi metriä pitkällä kaapelilla.',
    imagesrc: require("../assets/images/samsung.jpeg"),
    height:100,
    width:120,
    price:"1 799,00 €"
  }
];

export default class AccordionView extends React.Component {
  _renderSectionTitle(section) {
    return (
        <Row style={{height:140}}>
            <Col>
              <Row style={{height:80}}>
              <View style={{padding: 10,backgroundColor:'#fff',width:'100%'}}>
                  <Text style={{fontWeight:'600',fontSize:16}}>{section.content}</Text>
              </View>
              </Row>
              <Row style={{height:40}}>
                <Text style={{fontWeight:'700',fontSize:32,marginLeft:20,color:"#c62828"}}>{section.price}</Text>
              </Row>
            </Col>
            <Col style={{width:120,backgroundColor:'#fff'}}>
                <Row/>
                <Row style={{height:section.height}}><Col/>
                <Col style={{width:section.width}}>
                    <Image
                    source={
                        __DEV__
                        ? section.imagesrc
                        : section.imagesrc
                    }
                    style={{width:section.width,height:section.height,padding:10}}
                    />
                </Col><Col/></Row><Row/></Col>
        </Row>
    );
  }
  _renderHeader(section) {
    return (
    <Row style={{height:40,marginBottom:30}}>
      <View style={{backgroundColor:'#212121',padding:10,width:'100%',
      height:40, borderRadius:5}}>
        <Text style={{height:40,textAlign:'center',fontSize:16,fontWeight:'500',color:'#fff'}}>
        {section.title}</Text>
      </View>
    </Row>
    );
  }
  _renderContent(section) {
    return (
    <Row style={{minHeight:30}}>
      <View style={styles.content}>
        <Text>{section.moreContent}</Text>
      </View>
    </Row>
    );
  }
  render() {
    return (
        <Accordion style={{width:'100%',flex:1}} 
          sections={SECTIONS}
          renderSectionTitle={this._renderSectionTitle}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent}
        />
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF'
      },
      title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20
      },
      header: {
        backgroundColor: '#F5FCFF',
        padding: 10,
        width:'100%'
      },
      headerText: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '500'
      },
      content: {
        padding: 20,
        backgroundColor: '#fff',
        width:'100%'
      },
      active: {
        backgroundColor: 'rgba(255,255,255,1)'
      },
      inactive: {
        backgroundColor: 'rgba(245,252,255,1)'
      },
      selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center'
      },
      selector: {
        backgroundColor: '#F5FCFF',
        padding: 10
      },
      activeSelector: {
        fontWeight: 'bold'
      },
      selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10
    }
});
