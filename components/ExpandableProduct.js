import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { WebBrowser } from 'expo';


export default class ExpandableProduct extends React.Component {

  constructor(props){
    super(props)
    _onStateChange = this._onStateChange.bind(this)
  }
  state={
    toggle:false
  }
  _onPress(){
    const newState = !this.state.toggle;
    this.setState({toggle:newState})
    this.props.onStateChange && this.props.onStateChange(newState)
  }
  render() {
    const {toggle} = this.state;
    const textValue = toggle?"ON":"OFF";
    const buttonBg = toggle?"dodgerblue":"white";
    const textColor = toggle?"white":"black";
    const expandHeight = toggle?50:100;
    const showExtra = toggle?0:1;
    return (
        <View>
            <View onPress={()=>this._onPress()} style={{height:expandHeight}}>
            <Text style={{color='#000',fontSize=24}}>This text shows always</Text>
            <Text style={{opacity:showExtra,color='#000',fontSize=24}}> 
            This text shows on button press</Text>
            </View>
        </View>
    );
  }
}