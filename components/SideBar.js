import React from "react";
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Header,
    Alert,
    Button,
    FlatList,
    StatusBar} from 'react-native';

export default class SideBar extends React.Component {
    constructor(props){
      super(props)
    }
    render() {
      return (
          <View style={{backgroundColor:'#fff',height:'100%'}}>
              <View>
              <FlatList
                data={[
                    {key: 'Audio ja hifi'},
                    {key: 'Grillaus ja kokkaus'},
                    {key: 'Kaapelit'},
                    {key: 'Kamerat'},
                    {key: 'Kellot'},
                    {key: 'Kodinkoneet'},
                    {key: 'Komponentit'},
                    {key: 'Koti ja valaistus'},
                    {key: 'Laukut ja matkailu'},
                    {key: 'Lelut'},
                    {key: 'Lemmikit'},
                    {key: 'Musiikki'},
                    {key: 'Muut tuotteet'},
                    {key: 'Oheislaitteet'},
                    {key: 'Ohjelmistot'},
                    {key: 'Pelit ja viihde'},
                    {key: 'Pienkoneet'},
                    {key: 'Puhelimet'},
                    {key: 'Ruoka ja juoma'},
                    {key: 'Tarvike ja toimisto'},
                    {key: 'Tietokoneet'},
                    {key: 'TV ja video'},
                    {key: 'Urheilu'},
                    {key: 'Vauvat ja perhe'},
                    {key: 'Verkko'},
                    {key: 'Yritysmyynti'},
                    {key: 'Asennuspalvelut'},
                    {key: 'Huoltopalvelut'},
                    {key: 'Liittymät'},
                ]}
                renderItem={({item}) => 
                <Text style={{fontSize:14,margin:20}}>
                {item.key}</Text>}
                />
              </View>
          </View>
      );
    }
}