import React from "react";
import { StatusBar } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import SideBar from "../components/SideBar";
import { Container, Header, Title, Left, Icon, Right, Button, Body, 
  Content, Text, Card, CardItem, Drawer} from "native-base";
export default class HomeScreen extends React.Component {
  constructor(props){
    super(props)
  }
  closeDrawer = () => {
    this._drawer._root.close();
  }
  openDrawer = () => {
      this._drawer._root.open();
  }
  render() {
    return (
      <Drawer
        ref={(ref) => { this._drawer = ref; }}
        content={<SideBar />} >
        <StatusBar
            backgroundColor="black"
            barStyle="light-content"
        />
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={this.openDrawer.bind(this)}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>HomeScreen</Title>
          </Body>
          <Right />

        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>Chat App to talk some awesome people!</Text>
              </Body>
            </CardItem>
          </Card>
          <Button full rounded dark
            style={{ marginTop: 10 }}
            onPress={() => this.props.navigation.navigate("Chat")}>
            <Text>Chat With People</Text>
          </Button>
          <Button full rounded primary
            style={{ marginTop: 10 }}
            onPress={() => this.props.navigation.navigate("Profile")}>
            <Text>Goto Profiles</Text>
          </Button>
        </Content>
      </Container></Drawer>
    );
  }
}
