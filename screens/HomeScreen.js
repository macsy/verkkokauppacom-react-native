import React, { Component } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  StatusBar} from 'react-native';
  import { Container, Header, Title, Left, Icon, Right, Button, Body, 
    Content, Text, Card, CardItem, Drawer} from "native-base";
import { WebBrowser, Constants } from 'expo';
import { MonoText } from '../components/StyledText';
import { Col, Row, Grid } from "react-native-easy-grid";
import SimpleToggleButton from '../components/SimpleToggleButton';
import AccordionView from '../components/AccordionView';
import SideBar from "../components/SideBar";

export default class HomeScreen extends React.Component {
  constructor(props){
    super(props)
  }
  closeDrawer = () => {
    this._drawer._root.close();
  }
  openDrawer = () => {
      this._drawer._root.open();
  }
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <Drawer
        ref={(ref) => { this._drawer = ref; }}
        content={<SideBar />} ><Grid>
        <StatusBar
            backgroundColor="black"
            barStyle="light-content"
        />
        <View style={styles.container}>
        <Row style={{ height:100,width:'100%',zIndex:200 }}>
          <View style={styles.header}>
            <Row>
              <Button style={{zIndex:255,paddingRight:30,marginTop:3}}
                transparent
                onPress={this.openDrawer.bind(this)}>
                <Icon name="menu" style={{color:'#fff'}}/>
              </Button>
              <Col><Text style={styles.headertext}>Verkkokauppa.com React Native Demo</Text></Col>
            </Row>
          </View>
        </Row>
        <ScrollView>
          <Row style={{ minHeight:50,width:'100%',alignItems:'center',marginTop:30 }}>
            <Col style={{width:20}}/><Col><AccordionView/></Col><Col style={{width:20}}/>
          </Row>
          <Row/>
          <Row style = {{alignItems: 'center',marginTop:40,marginBottom:40}}>
            <Col/>
            <Col>
            <View style = {{alignItems: 'center'}}>
              <TouchableOpacity onPress={()=>{Alert.alert('You tapped the button!');}} >
                  <Text style = {styles.text}>
                    Alert Demo
                  </Text>
              </TouchableOpacity>
            </View>
            </Col>
            <Col/>
          </Row>
        </ScrollView>
      </View></Grid></Drawer>
    );
  }

  __toggleMenu(element){
    
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
           
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
  },
  welcomeImage: {
    width: 32,
    height: 32,
    marginTop: 10,
    resizeMode: 'contain',
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
    marginTop:125,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  header: {
    left: 0,
    top: 0,
    width: '100%',
    backgroundColor: '#000',
    height: 32,
    padding: 30,
    paddingBottom: 70,
  },
  headertext: {
    color: '#fff',
    fontSize: 20,
  },
  statusBar: {
    backgroundColor: "#C2185B",
    height: Constants.statusBarHeight,
  },
  cardContainer: {
    margin: 20,
  },
  productCard: {
    backgroundColor: '#fff'
  },
  productName: {
    color: '#000',
  },
  productPrice: {
    color: '#e53935',
  },
  text: {
    borderWidth: 1,
    padding: 15,
    borderColor: 'black',
    backgroundColor: 'red',
    color: '#fff',
    borderRadius:5
  }
});
